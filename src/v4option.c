/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "util.h"
#include "str2bin.h"
#include "v4v6option.h"

typedef struct {
    uint8_t option;
    const unsigned char* (*str2bin)(const char* value, uint32_t* length);
} dhcpv4_conv_t;

static const unsigned char* option_119_domain_search_list_to_hex(const char* value, uint32_t* length) {
    amxc_var_t dest;
    amxc_var_t src;
    const unsigned char* retval = NULL;

    amxc_var_init(&dest);
    amxc_var_init(&src);

    amxc_var_set(cstring_t, &src, value);

    when_false(dhcpoption_convertDomainNameList(&dest, &src, 0), exit);

    retval = str2bin_text(GET_CHAR(&dest, NULL), length);

exit:
    amxc_var_clean(&dest);
    amxc_var_clean(&src);
    return retval;
}

static dhcpv4_conv_t conversion[] = {
    {   1, str2bin_single_ipv4_addr },             // Subnet Mask
    {  16, str2bin_single_ipv4_addr },             // Swap Server
    {  28, str2bin_single_ipv4_addr },             // Broadcast Address
    {  32, str2bin_single_ipv4_addr },             // Router Solicitation Address
    {  50, str2bin_single_ipv4_addr },             // Requested IP Address
    {  54, str2bin_single_ipv4_addr },             // Server Identifier
    {   2, str2bin_int32 },                        // Time Offset
    {   3, str2bin_mult_ipv4_addr },               // Router
    {   4, str2bin_mult_ipv4_addr },               // Time Server
    {   5, str2bin_mult_ipv4_addr },               // Name Server
    {   6, str2bin_mult_ipv4_addr },               // Domain Name Server
    {   7, str2bin_mult_ipv4_addr },               // Log Server
    {   8, str2bin_mult_ipv4_addr },               // Cookie Server
    {   9, str2bin_mult_ipv4_addr },               // LPR Server
    {  10, str2bin_mult_ipv4_addr },               // Impress Server
    {  11, str2bin_mult_ipv4_addr },               // Resource Location Server
    {  41, str2bin_mult_ipv4_addr },               // Network Information Servers
    {  42, str2bin_mult_ipv4_addr },               // Network Time Protocol Servers
    {  44, str2bin_mult_ipv4_addr },               // NetBIOS over TCP/IP Name Server
    {  45, str2bin_mult_ipv4_addr },               // NetBIOS over TCP/IP Datagram Distribution Server
    {  48, str2bin_mult_ipv4_addr },               // X Window System Font Server
    {  49, str2bin_mult_ipv4_addr },               // X Window System Display Manager
    {  65, str2bin_mult_ipv4_addr },               // Network Information Service+ Servers
    {  68, str2bin_mult_ipv4_addr },               // Mobile IP Home Agent
    {  69, str2bin_mult_ipv4_addr },               // Simple Mail Transport Protocol (SMTP) Server
    {  70, str2bin_mult_ipv4_addr },               // Post Office Prostr_valuetocol (POP3) Server
    {  71, str2bin_mult_ipv4_addr },               // Network News Transport Protocol (NNTP) Server
    {  72, str2bin_mult_ipv4_addr },               // Default World Wide Web (WWW) Server
    {  73, str2bin_mult_ipv4_addr },               // Default Finger Server
    {  74, str2bin_mult_ipv4_addr },               // Default Internet Relay Chat (IRC) Server
    {  75, str2bin_mult_ipv4_addr },               // StreetTalk Server
    {  76, str2bin_mult_ipv4_addr },               // StreetTalk Directory Assistance (STDA) Server
    {  21, str2bin_mult_pairs_ipv4_addr },         // Policy Filter
    {  33, str2bin_mult_pairs_ipv4_addr },         // Static Route
    {  13, str2bin_single_uint16 },                // Boot File Size
    {  22, str2bin_single_uint16 },                // Maximum Datagram Reassembly Size
    {  26, str2bin_single_uint16 },                // Interface MTU
    {  57, str2bin_single_uint16 },                // Maximum DHCP Message Size
    {  19, str2bin_bool },                         // IP Forwarding Enable/Disable
    {  20, str2bin_bool },                         // Non-Local Source Routing Enable/Disable
    {  27, str2bin_bool },                         // All Subnets are Local Option
    {  29, str2bin_bool },                         // Perform Mask Discovery
    {  30, str2bin_bool },                         // Mask Supplier
    {  31, str2bin_bool },                         // Perform Router Discovery
    {  34, str2bin_bool },                         // Trailer Encapsulation
    {  36, str2bin_bool },                         // Ethernet Encapsulation
    {  39, str2bin_bool },                         // TCP Keepalive Garbage
    {  23, str2bin_uint8 },                        // Default IP Time-to-live
    {  37, str2bin_uint8 },                        // TCP Default TTL
    {  52, str2bin_uint8 },                        // Option Overload
    {  53, str2bin_uint8 },                        // DHCP Message Type
    {  24, str2bin_uint32 },                       // Path MTU Aging Timeout
    {  35, str2bin_uint32 },                       // ARP Cache Timeout
    {  38, str2bin_uint32 },                       // TCP Keepalive Interval
    {  51, str2bin_uint32 },                       // IP Address Lease Time
    {  58, str2bin_uint32 },                       // Renewal (T1) Time Value
    {  59, str2bin_uint32 },                       // Rebinding (T2) Time Value
    {  25, str2bin_mult_uint16 },                  // Path MTU Plateau Table
    {  12, str2bin_text },                         // Host Name
    {  14, str2bin_text },                         // Merit Dump File
    {  15, str2bin_text },                         // Domain Name
    {  17, str2bin_text },                         // Root Path
    {  18, str2bin_text },                         // Extensions Path
    {  40, str2bin_text },                         // Network Information Service Domain
    {  47, str2bin_text },                         // NetBIOS over TCP/IP Scope
    {  64, str2bin_text },                         // Network Information Service+ Domain
    {  66, str2bin_text },                         // TFTP server name
    {  67, str2bin_text },                         // Bootfile name
    {  56, str2bin_text },                         // Message
    {  43, str2bin_buffer },                       // Vendor Specific Information
    {  55, str2bin_buffer },                       // Parameter Request List - probably useless but just for completeness ...
    {  61, str2bin_buffer },                       // Client-identifier
    {  77, str2bin_buffer },                       // User class identifier
    {  90, str2bin_buffer },                       // Authentication
    { 119, option_119_domain_search_list_to_hex }, // Domain search
    { 120, str2bin_buffer },                       // Session Initiation Protocol (SIP) Servers
    { 121, str2bin_buffer },                       // Classless Static Route
    { 124, str2bin_buffer },                       // Vendor-Identifying Vendor Class
    { 125, str2bin_buffer },                       // Vendor-Identifying Vendor-Specific Information
    { 212, str2bin_buffer },                       // 6rd option
    { 252, str2bin_buffer },                       // Configure Mqtt server option
//
    {  46, str2bin_text },                         // Vendor class identifier
    {  60, str2bin_text },                         // Vendor class identifier
    { 255, NULL }
};

const unsigned char* dhcpoption_v4_str2bin(uint8_t option, const char* str_value, uint32_t* length) {
    dhcpv4_conv_t* search = conversion;
    const unsigned char* retval = NULL;
    when_null(str_value, exit);
    when_null(length, exit);

    while(search->option != 255) {
        if(search->option == option) {
            if(search->str2bin != NULL) {
                retval = search->str2bin(str_value, length);
            } else {
                // if not yet supported
                retval = str2bin_text(str_value, length);
            }
            break;
        }
        search++;
    }
    if(search->option == 255) {
        retval = str2bin_text("", length);
    }
exit:
    return retval;
}

void dhcpoption_v4parse(amxc_var_t* const parsed_lease_info, uint8_t option, uint32_t length, unsigned char* binval) {
    uint32_t i = 0;
    uint32_t m = 0;

    when_null(parsed_lease_info, exit);
    when_null(binval, exit);

    switch(option) {
    case 1:     // Subnet Mask
    case 16:    // Swap Server
    case 28:    // Broadcast Address
    case 32:    // Router Solicitation Address
    case 50:    // Requested IP Address
    case 54:    // Server Identifier
    {
        if(length >= 4) {
            amxc_var_set(cstring_t, parsed_lease_info, dhcpoption_parseIPv4Addr(binval));
        }
        break;
    }
    case 2:     // Time Offset
    {
        if(length >= 4) {
            amxc_var_set(int32_t, parsed_lease_info, dhcpoption_parseInt32(binval));
        }
        break;
    }
    case   3:     // Router
    case   4:     // Time Server
    case   5:     // Name Server
    case   6:     // Domain Name Server
    case   7:     // Log Server
    case   8:     // Cookie Server
    case   9:     // LPR Server
    case  10:     // Impress Server
    case  11:     // Resource Location Server
    case  41:     // Network Information Servers
    case  42:     // Network Time Protocol Servers
    case  44:     // NetBIOS over TCP/IP Name Server
    case  45:     // NetBIOS over TCP/IP Datagram Distribution Server
    case  48:     // X Window System Font Server
    case  49:     // X Window System Display Manager
    case  65:     // Network Information Service+ Servers
    case  68:     // Mobile IP Home Agent
    case  69:     // Simple Mail Transport Protocol (SMTP) Server
    case  70:     // Post Office Protocol (POP3) Server
    case  71:     // Network News Transport Protocol (NNTP) Server
    case  72:     // Default World Wide Web (WWW) Server
    case  73:     // Default Finger Server
    case  74:     // Default Internet Relay Chat (IRC) Server
    case  75:     // StreetTalk Server
    case  76:     // StreetTalk Directory Assistance (STDA) Server
    {
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        for(i = 0; i + 4 <= length; i += 4) {
            amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseIPv4Addr(&binval[i]));
        }
        break;
    }
    case  21:     // Policy Filter
    {
        amxc_var_t* map = NULL;
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);

        for(i = 0; i + 8 <= length; i += 8) {
            map = amxc_var_add(amxc_htable_t, parsed_lease_info, NULL);
            amxc_var_add_key(cstring_t, map, "Address", dhcpoption_parseIPv4Addr(&binval[i]));
            amxc_var_add_key(cstring_t, map, "Mask", dhcpoption_parseIPv4Addr(&binval[i + 4]));
        }
        break;
    }
    case  33:     // Static Route
    {
        amxc_var_t* map = NULL;
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);

        for(i = 0; i + 8 <= length; i += 8) {
            map = amxc_var_add(amxc_htable_t, parsed_lease_info, NULL);
            amxc_var_add_key(cstring_t, map, "Destination", dhcpoption_parseIPv4Addr(&binval[i]));
            amxc_var_add_key(cstring_t, map, "Router", dhcpoption_parseIPv4Addr(&binval[i + 4]));
        }
        break;
    }
    case  13:     // Boot File Size
    case  22:     // Maximum Datagram Reassembly Size
    case  26:     // Interface MTU
    case  57:     // Maximum DHCP Message Size
    {
        if(length < 2) {
            break;
        }
        amxc_var_set(uint16_t, parsed_lease_info, dhcpoption_parseUInt16(binval));
        break;
    }
    case  19:     // IP Forwarding Enable/Disable
    case  20:     // Non-Local Source Routing Enable/Disable
    case  27:     // All Subnets are Local Option
    case  29:     // Perform Mask Discovery
    case  30:     // Mask Supplier
    case  31:     // Perform Router Discovery
    case  34:     // Trailer Encapsulation
    case  36:     // Ethernet Encapsulation
    case  39:     // TCP Keepalive Garbage
    {
        if(length < 1) {
            break;
        }
        amxc_var_set(bool, parsed_lease_info, binval[0]);
        break;
    }
    case 23:     // Default IP Time-to-live
    case 37:     // TCP Default TTL
    case 52:     // Option Overload
    case 53:     // DHCP Message Type
    {
        if(length < 1) {
            break;
        }
        amxc_var_set(uint8_t, parsed_lease_info, binval[0]);
        break;
    }
    case  24:     // Path MTU Aging Timeout
    case  35:     // ARP Cache Timeout
    case  38:     // TCP Keepalive Interval
    case  51:     // IP Address Lease Time
    case  58:     // Renewal (T1) Time Value
    case  59:     // Rebinding (T2) Time Value
    {
        if(length < 4) {
            break;
        }
        amxc_var_set(uint32_t, parsed_lease_info, dhcpoption_parseUInt32(binval));
        break;
    }
    case  25:     // Path MTU Plateau Table
    {
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        for(i = 0; (i + 2) <= length; i += 2) {
            amxc_var_add(uint16_t, parsed_lease_info, dhcpoption_parseUInt16(&binval[i]));
        }
        break;
    }
    case 12:     // Host Name
    case 14:     // Merit Dump File
    case 15:     // Domain Name
    case 17:     // Root Path
    case 18:     // Extensions Path
    case 40:     // Network Information Service Domain
    case 47:     // NetBIOS over TCP/IP Scope
    case 64:     // Network Information Service+ Domain
    case 66:     // TFTP server name
    case 67:     // Bootfile name
    case 56:     // Message
    {
        amxc_var_set(cstring_t, parsed_lease_info, dhcpoption_parseAscii(binval, length));
        break;
    }
    case  43:     // Vendor Specific Information
    {
        amxc_var_t* map = NULL;
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);

        for(i = 0; i + 2 <= length && binval[i] != 255 && i + 2 + binval[i + 1] <= length; i += 2 + binval[i + 1]) {
            map = amxc_var_add(amxc_htable_t, parsed_lease_info, NULL);
            amxc_var_add_key(uint8_t, map, "Code", binval[i]);
            amxc_var_add_key(cstring_t, map, "Data", dhcpoption_parseDefault(&binval[i + 2], binval[i + 1]));
        }
        if((i < length) && (binval[i] == 255)) {
            i++;
        }
        if(i < length) {
            amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseDefault(&binval[i], length - i));
        }
        break;
    }
    case  55:     // Parameter Request List - probably useless but just for completeness ...
    {
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        for(i = 0; i < length; i++) {
            amxc_var_add(uint8_t, parsed_lease_info, binval[i]);
        }
        break;
    }
    case 61:     // Client-identifier
    {
        if(length < 1) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_lease_info, "Type", binval[0]);
        if((binval[0] == 48) && (length == 7)) { // any kind of hardware address
            amxc_var_add_key(cstring_t, parsed_lease_info, "ClientIdentifier", dhcpoption_parseLLAddress(&binval[1], length - 1));
        } else {                                 // unique identifier other than a hardware address
            amxc_var_add_key(cstring_t, parsed_lease_info, "ClientIdentifier", dhcpoption_parseDefault(&binval[1], length - 1));
        }
        break;
    }
    case 77:     // User class identifier
    {
        unsigned char s_len = binval[0];
        unsigned char* s = NULL;
        uint32_t j = 0;
        int slen = 0;

        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);

        if((s_len > 0) && (s_len < length)) {
            //option 77 allows a list of suboptions, each suboption starts with a length, followed by a suboption of that length.
            for(i = 0; i < length && i + 1 + binval[i] <= length; i += 1 + binval[i]) {
                amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseDefault(&binval[i + 1], binval[i]));
            }
        } else {
            //option 77 is not correctly configured. the option does not start with a length field. we allow it here for compatibility with older STB's filter out non printable chars
            s = (unsigned char*) calloc(1, length);
            for(j = 0; j < length; j++) {
                if(isprint(binval[j])) {
                    s[slen] = binval[j];
                    slen++;
                }
            }
            amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseDefault(s, slen));
            free(s);
        }
        break;
    }
    case  90:     // Authentication
    {
        if(length < 11) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_lease_info, "Protocol", binval[0]);
        amxc_var_add_key(uint8_t, parsed_lease_info, "Algorithm", binval[1]);
        amxc_var_add_key(uint8_t, parsed_lease_info, "RDM", binval[2]);
        amxc_var_add_key(cstring_t, parsed_lease_info, "ReplayDetection", dhcpoption_parseDefault(&binval[3], 8));
        amxc_var_add_key(cstring_t, parsed_lease_info, "AuthenticationInformation", dhcpoption_parseDefault(&binval[11], length - 11));
        break;
    }
    case 119:     //Domain search
    {
        uint32_t n = 0;
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        for(i = 0; i < length; i += n) {
            amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseDomainName(&binval[i], length - i, &n));
        }
        break;
    }
    case 120:     // Session Initiation Protocol (SIP) Servers
    {
        uint32_t n = 0;

        if(length < 1) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        if(binval[0] == 0) {      // list of DNS names
            for(i = 1; i < length; i += n) {
                amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseDomainName(&binval[i], length - i, &n));
            }
        } else if(binval[0] == 1) {    // list of IPv4 Addresses
            for(i = 1; (i + 4) <= length; i += 4) {
                amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseIPv4Addr(&binval[i]));
            }
        }
        break;
    }
    case 121:     // Classless Static Route
    {
        amxc_var_t* map = NULL;
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);

        for(i = 0; (i + 1) <= length; i += 4) {
            if((binval[i] > 32) || (i + (binval[i] + 7) / 8 + 4 >= length)) { // corrupt option format
                break;
            }
            unsigned char dst[4] = {'\0', '\0', '\0', '\0'};
            if(binval[i]) {
                memcpy(&dst, &binval[i + 1], (binval[i] + 7) / 8);
            }
            if(binval[i] % 8) {
                dst[(binval[i] - 1) / 8] &= 0xff00 >> (binval[i] % 8);
            }
            map = amxc_var_add(amxc_htable_t, parsed_lease_info, NULL);
            amxc_var_add_key(cstring_t, map, "Destination", dhcpoption_parseIPv4Addr(dst));
            amxc_var_add_key(uint8_t, map, "PrefixLength", binval[i]);
            i += 1 + (binval[i] + 7) / 8;
            amxc_var_add_key(cstring_t, map, "Router", dhcpoption_parseIPv4Addr(&binval[i]));
        }
        break;
    }
    case 124:     // Vendor-Identifying Vendor Class
    {
        amxc_var_t* map = NULL;
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);

        for(i = 0; i + 5 <= length && i + 5 + binval[i + 4] <= length; i += 5 + binval[i + 4]) {
            map = amxc_var_add(amxc_htable_t, parsed_lease_info, NULL);

            amxc_var_add_key(uint32_t, map, "Enterprise", dhcpoption_parseUInt32(&binval[i]));
            amxc_var_add_key(cstring_t, map, "Data", dhcpoption_parseDefault(&binval[i + 5], binval[i + 4]));
        }
        if((i < length) && (binval[i] == 255)) {
            i++;
        }
        if(i < length) {
            amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseDefault(&binval[i], length - i));
        }
        break;
    }
    case 125:     // Vendor-Identifying Vendor-Specific Information
    {
        amxc_var_t* map = NULL;
        amxc_var_t* sublist = NULL;
        amxc_var_t* submap = NULL;
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);

        for(i = 0; i + 5 <= length && i + 5 + binval[i + 4] <= length; i += 5 + binval[i + 4]) {
            map = amxc_var_add(amxc_htable_t, parsed_lease_info, NULL);
            amxc_var_add_key(uint32_t, map, "Enterprise", dhcpoption_parseUInt32(&binval[i]));
            sublist = amxc_var_add_key(amxc_llist_t, map, "Data", NULL);
            for(m = 0; m + 2 <= binval[i + 4] && m + 2 + binval[i + 5 + m + 1] <= binval[i + 4]; m += 2 + binval[i + 5 + m + 1]) {
                submap = amxc_var_add(amxc_htable_t, sublist, NULL);

                amxc_var_add_key(uint8_t, submap, "Code", binval[i + 5 + m]);
                amxc_var_add_key(cstring_t, submap, "Data", dhcpoption_parseDefault(&binval[i + 5 + m + 2], binval[i + 5 + m + 1]));
            }
            if(m < binval[i + 4]) {
                amxc_var_add(cstring_t, sublist, dhcpoption_parseDefault(&binval[i + 5 + m], binval[i + 4] - m));
            }
        }
        break;
    }
    case 212:     //6rd option
    {
        amxc_var_t* sublist = NULL;
        if(length < 22) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_lease_info, "IPv4MaskLen", binval[0]);
        amxc_var_add_key(uint8_t, parsed_lease_info, "6rdPrefixLen", binval[1]);
        amxc_var_add_key(cstring_t, parsed_lease_info, "6rdPrefix", dhcpoption_parseIPv6Addr(&binval[2]));

        sublist = amxc_var_add_key(amxc_llist_t, parsed_lease_info, "6rdBRIPv4Address", NULL);
        i = 0;
        while(21 + (i * 4) <= length) { //21 = 18(start location) + 3(size)
            amxc_var_add(cstring_t, sublist, dhcpoption_parseIPv4Addr(&binval[18 + (i * 4)]));
            i++;
        }

        break;
    }
    case 252:     // Configure Mqtt server option
    {
        if(length < 2) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_lease_info, "SubOption0", binval[0]);
        amxc_var_add_key(uint8_t, parsed_lease_info, "Networktype", binval[1]);
        break;
    }
    case 46:     // NetBIOS over TCP/IP Node Type Option - it has a specific format but who cares?
    case 60:     // Vendor class identifier
    default:     // unknown option - simple ascii string if all bytes between 32 and 126, keep hexbinary prefixed with 0x otherwise
    {
        amxc_var_set(cstring_t, parsed_lease_info, dhcpoption_parseDefault(binval, length));
        break;
    }
    }

exit:
    return;
}

unsigned char* dhcpoption_option_convert2bin(const char* hexstring, uint32_t* length) {
    // Hexstring should contain all bytes in hexa decimal value.
    // Each byte is represented by two chars

    unsigned char* data = NULL;
    char byte[3] = {0};
    uint32_t i = 0;

    when_null(hexstring, exit);
    when_null(length, exit);

    *length = strlen(hexstring);
    data = (unsigned char*) calloc(1, *length / 2);
    when_null(data, exit);

    for(i = 0; i < *length; i += 2) {
        byte[0] = hexstring[i];
        byte[1] = hexstring[i + 1];
        data[i / 2] = strtoul(byte, NULL, 16);
    }
    *length /= 2;

exit:
    return data;
}
