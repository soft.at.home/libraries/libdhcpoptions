/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

#include "util.h"
#include "v4v6option.h"
#include "str2bin.h"

typedef struct {
    uint32_t option;
    bool (* str2bin)(amxc_var_t* dest, amxc_var_t* src);
} dhcp6_conv_t;

static bool option_24_domain_search_list_to_hex(amxc_var_t* dest, amxc_var_t* src) {
    return (dhcpoption_convertDomainNameList(dest, src, 0) == 0);
}

static bool option_39_client_fqdn_to_hex(amxc_var_t* dest, amxc_var_t* src) {
    amxc_var_t copy;
    uint32_t length = 0;
    bool rv = false;

    amxc_var_init(&copy);
    amxc_var_copy(&copy, GET_ARG(src, "Domains"));

    when_failed(amxc_var_cast(&copy, AMXC_VAR_ID_LIST), exit);

    str2bin_uint8(GET_CHAR(src, "Flag"), &length);
    when_false(length == 1, exit);

    rv = dhcpoption_convertDomainNameList(dest, &copy, length);

exit:
    amxc_var_clean(&copy);
    return rv;
}

static bool option_64_aftr_to_hex(amxc_var_t* dest, amxc_var_t* src) {
    // option 64 is a list of domains with only 1 item
    return option_24_domain_search_list_to_hex(dest, src);
}

static dhcp6_conv_t conversion[] = {
    { 24, option_24_domain_search_list_to_hex },
    { 39, option_39_client_fqdn_to_hex },
    { 64, option_64_aftr_to_hex },
    { USHRT_MAX, NULL }
};

bool dhcpoption_v6_option_to_hex(uint16_t option, amxc_var_t* dest, amxc_var_t* src) {
    bool retval = false;

    when_null(dest, exit);
    when_null(src, exit);

    for(dhcp6_conv_t* search = &conversion[0]; search->option != USHRT_MAX; search++) {
        if(search->option == option) {
            retval = search->str2bin(dest, src);
            break;
        }
    }

exit:
    return retval;
}

static void dhcp_api_parseSubOptions(amxc_var_t* const list, uint32_t length, unsigned char* binval) {
    uint32_t i = 0;
    uint16_t type = 0;
    uint16_t olen = 0;
    amxc_var_t* map = NULL;
    amxc_var_t* value_placeholder = NULL;

    while(i + 4 <= length) {
        type = dhcpoption_parseUInt16(&binval[i]);
        olen = dhcpoption_parseUInt16(&binval[i + 2]);
        if(i + 4 + olen > length) {
            break;
        }
        map = amxc_var_add(amxc_htable_t, list, NULL);
        amxc_var_add_key(uint16_t, map, "Type", type);
        value_placeholder = amxc_var_add_key(amxc_llist_t, map, "Value", NULL);

        amxc_var_t* value_var = NULL;
        amxc_var_new(&value_var);

        dhcpoption_v6parse(value_var, type, olen, &binval[i + 4]);
        amxc_var_move(value_placeholder, value_var);

        amxc_var_delete(&value_var);
        i += 4 + olen;
    }
}

void dhcpoption_v6parse(amxc_var_t* const parsed_lease_info, uint16_t option, uint32_t length, unsigned char* binval) {
    uint32_t i = 0;

    when_null(parsed_lease_info, exit);
    when_null(binval, exit);

    switch(option) {
    case   1:     // Client Identifier
    case   2:     // Server Identifier
    {
        uint16_t type = 0;
        if(length < 2) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        type = dhcpoption_parseUInt16(binval);
        amxc_var_add_key(uint16_t, parsed_lease_info, "Type", type);
        switch(type) {
        case 1:         // DUID-LLT
            if(length < 8) {
                break;
            }
            amxc_var_add_key(uint16_t, parsed_lease_info, "HardwareType", dhcpoption_parseUInt16(&binval[2]));
            amxc_var_add_key(uint32_t, parsed_lease_info, "Time", dhcpoption_parseUInt32(&binval[4]));
            amxc_var_add_key(cstring_t, parsed_lease_info, "LLAddress", dhcpoption_parseLLAddress(&binval[8], length - 8));
            break;
        case 2:         // DUID-EN
            if(length < 6) {
                break;
            }
            amxc_var_add_key(uint32_t, parsed_lease_info, "Enterprise", dhcpoption_parseUInt32(&binval[2]));
            amxc_var_add_key(cstring_t, parsed_lease_info, "Identifier", dhcpoption_parseDefault(&binval[6], length - 6));
            break;
        case 3:         // DUID-LL
            if(length < 4) {
                break;
            }
            amxc_var_add_key(uint16_t, parsed_lease_info, "HardwareType", dhcpoption_parseUInt16(&binval[2]));
            amxc_var_add_key(cstring_t, parsed_lease_info, "LLAddress", dhcpoption_parseLLAddress(&binval[4], length - 4));
            break;
        default:
            amxc_var_add_key(cstring_t, parsed_lease_info, "Data", dhcpoption_parseDefault(&binval[2], length - 2));
            break;
        }
        break;
    }
    case   3:     // Identity Association for Non-temporary Addresses
    case  25:     // Identity Association for Prefix Delegation
    {
        amxc_var_t* list = NULL;
        if(length < 12) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, parsed_lease_info, "IAID", dhcpoption_parseUInt32(binval));
        amxc_var_add_key(uint32_t, parsed_lease_info, "T1", dhcpoption_parseUInt32(&binval[4]));
        amxc_var_add_key(uint32_t, parsed_lease_info, "T2", dhcpoption_parseUInt32(&binval[8]));

        list = amxc_var_add_key(amxc_llist_t, parsed_lease_info, "Options", NULL);
        dhcp_api_parseSubOptions(list, length - 12, &binval[12]);
        break;
    }
    case   4:     // Identity Association for Temporary Addresses
    {
        amxc_var_t* list = NULL;
        if(length < 4) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, parsed_lease_info, "IAID", dhcpoption_parseUInt32(binval));

        list = amxc_var_add_key(amxc_llist_t, parsed_lease_info, "Options", NULL);
        dhcp_api_parseSubOptions(list, length - 4, &binval[4]);
        break;
    }
    case   5:     // IA Address
    {
        amxc_var_t* list = NULL;
        if(length < 24) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, parsed_lease_info, "Address", dhcpoption_parseIPv6Addr(binval));
        amxc_var_add_key(uint32_t, parsed_lease_info, "PreferredLifetime", dhcpoption_parseUInt32(&binval[16]));
        amxc_var_add_key(uint32_t, parsed_lease_info, "ValidLifetime", dhcpoption_parseUInt32(&binval[20]));

        list = amxc_var_add_key(amxc_llist_t, parsed_lease_info, "Options", NULL);
        dhcp_api_parseSubOptions(list, length - 24, &binval[24]);

        break;
    }
    case   6:     // Option Request
    case  43:     // Echo Request
    {
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        for(i = 0; i + 2 <= length; i += 2) {
            amxc_var_add(uint16_t, parsed_lease_info, dhcpoption_parseUInt16(&binval[i]));
        }
        break;
    }
    case   7:     // Preference
    case  19:     // Reconfigure Message
    {
        if(length < 1) {
            break;
        }
        amxc_var_set(uint8_t, parsed_lease_info, binval[0]);
        break;
    }
    case   8:     // Elapsed Time
    {
        if(length < 2) {
            break;
        }
        amxc_var_set(uint16_t, parsed_lease_info, dhcpoption_parseUInt16(binval));
        break;
    }
    case   9:     // Relay Message
    {
        amxc_var_set(cstring_t, parsed_lease_info, dhcpoption_parseHex(binval, length));
        break;
    }
    case  11:     // Authentication
    {
        if(length < 11) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_lease_info, "Protocol", binval[0]);
        amxc_var_add_key(uint8_t, parsed_lease_info, "Algorithm", binval[1]);
        amxc_var_add_key(uint8_t, parsed_lease_info, "RDM", binval[2]);
        amxc_var_add_key(cstring_t, parsed_lease_info, "ReplayDetection", dhcpoption_parseDefault(&binval[3], 8));
        amxc_var_add_key(cstring_t, parsed_lease_info, "AuthenticationInformation", dhcpoption_parseDefault(&binval[11], length - 11));
        break;
    }
    case  12:     // Server Unicast
    {
        if(length < 16) {
            break;
        }
        amxc_var_set(cstring_t, parsed_lease_info, dhcpoption_parseIPv6Addr(binval));
        break;
    }
    case  13:     // Status Code
    {
        if(length < 2) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint16_t, parsed_lease_info, "Code", dhcpoption_parseUInt16(binval));
        amxc_var_add_key(cstring_t, parsed_lease_info, "Message", (char*) &binval[2]);
        break;
    }
    case  14:     // Rapid Commit
    case  20:     // Reconfigure Accept
    {
        amxc_var_set(bool, parsed_lease_info, true);
        break;
    }
    case  15:     // User Class
    {
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        for(i = 0; i + 2 <= length && i + 2 + dhcpoption_parseUInt16(&binval[i]) <= length; i += 2 + dhcpoption_parseUInt16(&binval[i])) {
            amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseDefault(&binval[i + 2], dhcpoption_parseUInt16(&binval[i])));
        }
        break;
    }
    case  17:     // Vendor-specific Information
    {
        amxc_var_t* list = NULL;
        amxc_var_t* map = NULL;
        if(length < 4) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, parsed_lease_info, "Enterprise", dhcpoption_parseUInt32(binval));
        list = amxc_var_add_key(amxc_llist_t, parsed_lease_info, "Data", NULL);
        for(i = 4; i + 4 <= length && i + 4 + dhcpoption_parseUInt16(&binval[i + 2]) <= length; i += 4 + dhcpoption_parseUInt16(&binval[i + 2])) {
            map = amxc_var_add(amxc_htable_t, list, NULL);
            amxc_var_add_key(uint8_t, map, "Code", dhcpoption_parseUInt16(&binval[i]));
            amxc_var_add_key(cstring_t, map, "Data", dhcpoption_parseDefault(&binval[i + 4], dhcpoption_parseUInt16(&binval[i + 2])));
        }
        break;
    }
    case  21:     // SIP Servers Domain Name List
    case  24:     // Domain Search List
    case  33:     // Broadcast and Multicast Service Controller Domain Name List
    case  58:     // SIP User Agent Configuration Service Domains
    {
        uint32_t n = 0;
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        for(i = 0; i < length; i += n) {
            amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseDomainName(&binval[i], length - i, &n));
        }
        break;
    }
    case  22:     // SIP Servers IPv6 Address List
    case  23:     // DNS Recursive Name Server
    case  27:     // Network Information Service (NIS) Servers
    case  28:     // Network Information Service V2 (NIS+) Servers
    case  31:     // Simple Network Time Protocol (SNTP) Servers
    case  34:     // Broadcast and Multicast Service Controller IPv6 Address
    case  40:     // PANA Authentication Agent
    case  48:     // Leasequery Client Link
    {
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        for(i = 0; i + 16 <= length; i += 16) {
            amxc_var_add(cstring_t, parsed_lease_info, dhcpoption_parseIPv6Addr(&binval[i]));
        }
        break;
    }
    case  26:     // IA_PD Prefix
    {
        amxc_var_t* list = NULL;
        if(length < 25) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, parsed_lease_info, "PreferredLifetime", dhcpoption_parseUInt32(binval));
        amxc_var_add_key(uint32_t, parsed_lease_info, "ValidLifetime", dhcpoption_parseUInt32(&binval[4]));
        amxc_var_add_key(uint8_t, parsed_lease_info, "PrefixLen", binval[8]);
        amxc_var_add_key(cstring_t, parsed_lease_info, "Prefix", dhcpoption_parseIPv6Addr(&binval[9]));

        list = amxc_var_add_key(amxc_llist_t, parsed_lease_info, "Options", NULL);
        dhcp_api_parseSubOptions(list, length - 25, &binval[25]);
        break;
    }
    case  29:     // Network Information Service (NIS) Domain Name
    case  30:     // Network Information Service V2 (NIS+) Domain Name
    case  51:     // LoST server
    case  64:     // AFTR-Name for DSLite
    {
        amxc_var_set(cstring_t, parsed_lease_info, dhcpoption_parseDomainName(binval, length, NULL));
        break;
    }
    case  32:     // Information Refresh Time
    case  46:     // Leasequery Client Transaction time
    {
        if(length < 4) {
            break;
        }
        amxc_var_set(uint32_t, parsed_lease_info, dhcpoption_parseUInt32(binval));
        break;
    }
    case  36:     // Civic Addresses Configuration Information
    {
        amxc_var_t* list = NULL;
        amxc_var_t* map = NULL;
        if(length < 3) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_lease_info, "What", binval[0]);

        char country[3] = { (char) binval[1], (char) binval[2], '\0' };
        amxc_var_add_key(cstring_t, parsed_lease_info, "Country", country);

        list = amxc_var_add_key(amxc_llist_t, parsed_lease_info, "Address", NULL);
        for(i = 3; i + 2 <= length && i + 2 + binval[i + 1] <= length; i += 2 + binval[i + 1]) {
            map = amxc_var_add(amxc_htable_t, list, NULL);
            amxc_var_add_key(uint8_t, map, "Type", binval[i]);
            amxc_var_add_key(cstring_t, map, "Value", dhcpoption_parseAscii(&binval[i + 2], binval[i + 1]));
        }
        break;
    }
    case 37:     // Relay Agent Remote-ID
    {
        if(length < 5) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, parsed_lease_info, "Enterprise", dhcpoption_parseUInt32(binval));
        amxc_var_add_key(cstring_t, parsed_lease_info, "RemoteID", dhcpoption_parseDefault(&binval[4], length - 4));
        break;
    }
    case 39:     // Client FQDN
    {
        if(length < 1) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_lease_info, "Flags", binval[0]);
        amxc_var_add_key(cstring_t, parsed_lease_info, "DomainName", dhcpoption_parseDomainName(&binval[1], length - 1, NULL));
        break;
    }
    case  41:     // Timezone POSIX String
    case  42:     // Timezone Name
    {
        amxc_var_set(cstring_t, parsed_lease_info, (char*) binval);
        break;
    }
    case  44:     // Leasequery Query
    {
        amxc_var_t* list = NULL;
        if(length < 17) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint8_t, parsed_lease_info, "Type", binval[0]);
        amxc_var_add_key(cstring_t, parsed_lease_info, "LinkAddress", dhcpoption_parseIPv6Addr(&binval[1]));

        list = amxc_var_add_key(amxc_llist_t, parsed_lease_info, "Options", NULL);
        dhcp_api_parseSubOptions(list, length - 17, &binval[17]);
        break;
    }
    case  45:     // Leasequery Client Data
    {
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_LIST);
        dhcp_api_parseSubOptions(parsed_lease_info, length, binval);
        break;
    }
    case  47:     // Leasequery Relay Data
    {
        if(length < 16) {
            break;
        }
        amxc_var_set_type(parsed_lease_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, parsed_lease_info, "PeerAddress", dhcpoption_parseIPv6Addr(binval));
        amxc_var_add_key(cstring_t, parsed_lease_info, "Message", dhcpoption_parseHex(&binval[16], length - 16));
        break;
    }
    case  18:    // Interface-Id
    case  38:    // Relay Agent Subscriber-ID
    default:     // unknown option - simple ascii string if all bytes between 32 and 126, keep hexbinary prefixed with 0x otherwise
    {
        amxc_var_set(cstring_t, parsed_lease_info, dhcpoption_parseDefault(binval, length));
        break;
    }
    }

exit:
    return;
}

bool dhcpoption_v6allowMultiple(uint8_t tag) {
    switch(tag) {
    case   3:     // Identity Association for Non-temporary Addresses
    case   4:     // Identity Association for Temporary Addresses
    case  17:     // Vendor-specific Information
    case  25:     // Identity Association for Prefix Delegation
        return true;
    default:
        return false;
    }
}
