# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.6.0 - 2023-07-14(10:16:57 +0000)

### New

- [tr181-logical][LAN mib] The Lan mib must expose the domainSearchList parameter

## Release v0.5.1 - 2023-05-11(12:43:46 +0000)

### Fixes

- [DHCPv6Client] Cannot set SentOption Vendor Class (option 16)

## Release v0.5.0 - 2023-01-26(07:53:01 +0000)

### New

- update DNS data model with hosts from DHCP server pool

## Release v0.4.0 - 2022-10-06(07:26:00 +0000)

### New

- support option AFTR

## Release v0.3.1 - 2022-05-19(12:43:23 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.3.0 - 2022-02-03(19:49:10 +0000)

### New

- [lib DHCPOptions] Add parsing code for router advertisement options

## Release v0.2.0 - 2021-12-14(10:07:04 +0000)

### New

- Added string to binary conversion for use in DHCPv4 client

## Release v0.1.6 - 2021-09-14(07:37:38 +0000)

### Fixes

- Add version to `.so` file

## Release v0.1.5 - 2021-09-01(11:49:56 +0000)

### Other

- [CI] Disable gitlab pages job
- [CI] Disable test job
- [CI] Enable package generation job

## Release v0.1.4 - 2021-09-01(08:57:48 +0000)

### Other

- Component '.so' should be versioned

## Release v0.1.3 - 2021-08-31(11:09:53 +0000)

### Fixes

- Add debian packages

## Release v0.1.2 - 2021-08-31(09:07:55 +0000)

### Fixes

- change name

## Release v0.1.1 - 2021-08-30(08:56:40 +0000)

### Fixes

- update dependencies

## Release v0.1.0 - 2021-08-26(14:55:03 +0000)

### New

- implement the module libdhcpoption

