#include <amxc/amxc.h>
#include "v4v6option.h"

int LLVMFuzzerTestOneInput(const uint8_t* data, size_t len) {
    amxc_var_t var;

    amxc_var_init(&var);

    dhcpoption_v6parse(&var, data[0], len, (unsigned char*) &data[len > 1 ? 1 : 0]);

    amxc_var_clean(&var);
    return 0;  // Values other than 0 and -1 are reserved for future use.
}